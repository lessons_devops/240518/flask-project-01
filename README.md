  О проекте Flask-project:    
 
   Kubernetes:       
1. Автоматическая публикация чарта в Gitlab "Package Registry"    
2. Создание и управление окружениями (dev, staging, prod) в Gitlab и Kubernetes (с помошью Helm чартов)    
3. Горизонтальное автоскалирование (HPA)     
4. Отменить изминение или Откат (Rollback).    
   4.1 С помощью "Deployment history" в Environments и заного происходит предыдуший деплой приложение в кластер.    
   4.2 С помощью функционалу в Pipelines, указав номер ревизии (не указав значение, он вернет на -1 ревизию).    
5. Включена шифрование секретов в minikube.   

Тестовый стенд для проекта:    

Хост-1:     
Ubuntu - 22.04    
minikube version: v1.31.2    
VM Driver = docker     
На хосте установлен раннер Gitlab:    
Gitlab-runner = shell    
    
Хост-2:     
Gitlab-runner = shell    
Ubuntu - 22.04    
Используется для сборки докера и хелм чартов     
(можно все выполнять на одном хосте)    

В данном случае тип ранеров shell стоит для улудшение быстродейстие сборки и деплоя (в тестовых целях)     

Управление CI/CD в репозиторием [Flask-project01](https://gitlab.com/lessons_devops/240518/flask-project-01) выполняется из [gitlab_flow_flask](https://gitlab.com/lessons_devops/240518/gitlab_flows)      
А в данном репозитории указан файл в настройка CI/CD: [project-flask-cicd.yml](https://gitlab.com/lessons_devops/240518/gitlab_flows/-/blob/main/project-flask-cicd.yml)    

   "CI/CD configuration file": project-flask-cicd.yml@lessons_devops/240518/gitlab_flows

Репозитории:  
Control CI/CD [gitlab_flow_flask](https://gitlab.com/lessons_devops/240518/gitlab_flows)    
Helm [helm_flask](https://gitlab.com/lessons_devops/240518/helm_flask)   

Сборка docker-контейнера : [Dockerfile](https://gitlab.com/lessons_devops/240518/flask-project-01/-/blob/main/dockerfile)    

Images приложения в репозиториях:   
- Hub-docker  Images: [Flask-project](https://hub.docker.com/r/ganebaldenis/flask-project/tags)    
- Gitlab  Container_registry: [Flask-project](https://gitlab.com/lessons_devops/240518/flask-project-01/container_registry/6587875)    

Версионирование:    
1. В Helm репозитории автоматическое увелечение версии чарта (не приложение). ([helm_flask](https://gitlab.com/lessons_devops/240518/helm_flask))    
2. В проекте "Flask-project" добавлена возможность автоматическое увелечение версии приложение     
при каждом коммите (для тестовых нужд)    

appVersion  I++ :    
Этап в пайплайне (Job) : .increment_version_app  # доп. коммит для измение версии в файле.    
Этап в пайплайне (Job) : .helm-add-appVersion    # изминение в Chart.yaml appVersion    
Файл в репоз.: [version_i++.sh](https://gitlab.com/lessons_devops/240518/flask-project-01/-/blob/main/version_i++.sh)    

Секреты передаются по средством Gitlab при деплое, создается объект "secret".    

Примечание:

1. Для окружение продакшен нужно использовать базу данных на отдельном хосте не в кубере    
2. Для окружение staging использовать БД с коппией базы данных и инфраструктурой приблеженно к продакшен, но с минимальными ресурсами     
3. Для окружение develope можно использовать базу данных в кубере    

 

Screenshots:
  Проверки работы приложение, и запрос данных с БД:    

   <img src="/screenshots/curl-test.png"  width="500" height="500">    

1. Автоматическая публикация чарта в Gitlab "Package Registry"   
    
   <img src="/screenshots/helm-pub.png"  width="500" height="500">   
  
2. Создание и управление окружениями (dev, staging, prod) в Gitlab и кубернетисе (с помошью Helm чартов)

   <img src="/screenshots/gitlab-Environments.png"  width="900" height="900">


3. Горизонтальное автоскалирование (HPA)    
   <img src="/screenshots/hpa-testing.png"  width="500" height="500">

   <img src="/screenshots/hpa-list-pods.png"  width="500" height="500">

4. Отменить изминение или Откат (Rollback).      
   4.1 С помощью "Deployment history" в Environments и заного происходит предыдуший деплой приложение в кластер.  

    <img src="/screenshots/gitlab-rollback.png"  width="500" height="500">
	
   4.2 С помощью функционалу в Pipelines, указав номер ревизии (не указав значение, он вернет на -1 ревизию).     

    <img src="/screenshots/revisions-run.png"  width="500" height="500">

    <img src="/screenshots/revision-1.png"  width="500" height="500">
    
    <img src="/screenshots/helm-history-rollback.png"  width="500" height="500">

 5. Включена шифрование секретов в minikube:

  <img src="/screenshots/kuber-secrets-encoded.png"  width="500" height="500">

  <img src="/screenshots/kuber-secrets-open.png"  width="500" height="500">

Репозиторий с images  

<img src="/screenshots/container-reg.png"  width="500" height="500">

Gitlab переменые для каждого окружение:   

<img src="/screenshots/gitlab-variables.png"  width="500" height="500">


<img src="/screenshots/gitlab-run pipeline var.png"  width="500" height="500">
 


