FROM python:3.8-slim as builder
ENV PYTHONUNBUFFERED=1
RUN apt-get update && apt-get install -y --no-install-recommends \
    gcc \
    libpq-dev \
    && rm -rf /var/lib/apt/lists/*
# Создание директории для wheel-пакетов
WORKDIR /wheels
COPY ./requirements-server.txt /requirements-server.txt
# Создание wheel-пакетов для зависимостей
RUN pip install -U pip setuptools wheel \
    && pip wheel -r /requirements-server.txt

FROM python:3.8-slim
ENV PYTHONUNBUFFERED=1
RUN apt-get update && apt-get install -y --no-install-recommends \
    libpq-dev \
    && rm -rf /var/lib/apt/lists/*
# Копирование wheel-пакетов из builder-стадии
COPY --from=builder /wheels /wheels
# Установка зависимостей из wheel-пакетов
RUN pip install -U pip setuptools wheel \
    && pip install /wheels/* \
    && rm -rf /wheels \
    && rm -rf /root/.cache/pip/*
WORKDIR /app
COPY . .
EXPOSE 8000
CMD ["gunicorn", "app:app", "-b", "0.0.0.0:8000"]
