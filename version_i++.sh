#!/bin/bash

VERSION_FILE="auto_version_app.txt"         # Путь к файлу с версией
CURRENT_VERSION=$(cat $VERSION_FILE)        # Чтение текущей версии из файла
IFS='.' read -r -a VERSION_PARTS <<< "$CURRENT_VERSION" # Разбиение версии на компоненты
VERSION_PARTS[2]=$((VERSION_PARTS[2] + 1))  # Инкрементируем последнюю часть версии
NEW_VERSION="${VERSION_PARTS[0]}.${VERSION_PARTS[1]}.${VERSION_PARTS[2]}" # Формируем новую версию
echo $NEW_VERSION > $VERSION_FILE           # Записываем новую версию обратно в файл
echo "New version: $NEW_VERSION"            # Выводим новую версию (может пригодиться в CI/CD процессе)

